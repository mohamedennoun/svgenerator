#ifndef _POLYGON_HPP_
#define _POLYGON_HPP_
#include "Shape.hpp"

class Polygon : public Shape
{
public:
  Polygon(Fill const &fill = Fill(), Stroke const &stroke = Stroke())
      : Shape(fill, stroke) {}
  Polygon(Stroke const &stroke = Stroke()) : Shape(Color::Transparent, stroke) {}
  Polygon &operator<<(Point const &point)
  {
    points.push_back(point);
    return *this;
  }
  std::string toString(Layout const &layout) const
  {
    std::stringstream ss;
    ss << elemStart("polygon");

    ss << "points=\"";
    for (unsigned i = 0; i < points.size(); ++i)
      ss << translateX(points[i].x, layout) << "," << translateY(points[i].y, layout) << " ";
    ss << "\" ";

    ss << fill.toString(layout) << stroke.toString(layout) << emptyElemEnd();
    return ss.str();
  }
  void offset(Point const &offset)
  {
    for (unsigned i = 0; i < points.size(); ++i)
    {
      points[i].x += offset.x;
      points[i].y += offset.y;
    }
  }

private:
  std::vector<Point> points;
};

#endif