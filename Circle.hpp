#ifndef _CIRCLE_HPP_
#define _CIRCLE_HPP_

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include "Shape.hpp"
#include "Point.hpp"
#include "Stroke.hpp"
#include "Layout.hpp"
class Circle : public Shape
{
public:
    Circle(Point const &center, double diameter, Fill const &fill,
           Stroke const &stroke = Stroke())
        : Shape(fill, stroke), center(center), radius(diameter / 2) {}
    std::string toString(Layout const &layout) const
    {
        std::stringstream ss;
        ss << elemStart("circle") << attribute("cx", translateX(center.x, layout))
           << attribute("cy", translateY(center.y, layout))
           << attribute("r", translateScale(radius, layout)) << fill.toString(layout)
           << stroke.toString(layout) << emptyElemEnd();
        return ss.str();
    }
    void offset(Point const &offset)
    {
        center.x += offset.x;
        center.y += offset.y;
    }

private:
    Point center;
    double radius;
};

#endif