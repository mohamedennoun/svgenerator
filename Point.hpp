#ifndef _POINT_HPP_     	  			  	  	 
#define _POINT_HPP_   
  
 struct Point
    {
        Point(double x = 0, double y = 0) : x(x), y(y) { }
        double x;
        double y;
    };
    
#endif