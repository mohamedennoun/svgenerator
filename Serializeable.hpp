#ifndef _SERIALIZABLE_HPP_
#define _SERIALIZABLE_HPP_
#include "stdlib.h"
#include <string>
#include "Layout.hpp"

class Serializeable
{
public:
  Serializeable() {}
  virtual ~Serializeable(){};
  virtual std::string toString(Layout const &layout) const = 0;
};

#endif