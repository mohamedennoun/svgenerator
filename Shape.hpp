
#ifndef _SHAPE_HPP_
#define _SHAPE_HPP_

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include "Serializeable.hpp"
#include "Fill.hpp"
#include "Stroke.hpp"
#include "Point.hpp"

class Shape : public Serializeable
{
public:
    Shape(Fill const &fill = Fill(), Stroke const &stroke = Stroke())
        : fill(fill), stroke(stroke) {}
    virtual ~Shape() {}
    virtual std::string toString(Layout const &layout) const = 0;
    virtual void offset(Point const &offset) = 0;

protected:
    Fill fill;
    Stroke stroke;
};
template <typename T>
std::string vectorToString(std::vector<T> collection, Layout const &layout)
{
    std::string combination_str;
    for (unsigned i = 0; i < collection.size(); ++i)
        combination_str += collection[i].toString(layout);

    return combination_str;
}

#endif