#ifndef _TEXT_HPP_
#define _TEXT_HPP_
#include "Shape.hpp"
#include "Font.hpp"

class Text : public Shape
{
public:
  Text(Point const &origin, std::string const &content, Fill const &fill = Fill(),
       Font const &font = Font(), Stroke const &stroke = Stroke())
      : Shape(fill, stroke), origin(origin), content(content), font(font) {}
  std::string toString(Layout const &layout) const
  {
    std::stringstream ss;
    ss << elemStart("text") << attribute("x", translateX(origin.x, layout))
       << attribute("y", translateY(origin.y, layout))
       << fill.toString(layout) << stroke.toString(layout) << font.toString(layout)
       << ">" << content << elemEnd("text");
    return ss.str();
  }
  void offset(Point const &offset)
  {
    origin.x += offset.x;
    origin.y += offset.y;
  }

private:
  Point origin;
  std::string content;
  Font font;
};

#endif