#ifndef _LINE_HPP_
#define _LINE_HPP_
#include "Shape.hpp"

class Line : public Shape
{
public:
  Line(Point const &start_point, Point const &end_point,
       Stroke const &stroke = Stroke())
      : Shape(Fill(), stroke), start_point(start_point),
        end_point(end_point) {}
  std::string toString(Layout const &layout) const
  {
    std::stringstream ss;
    ss << elemStart("line") << attribute("x1", translateX(start_point.x, layout))
       << attribute("y1", translateY(start_point.y, layout))
       << attribute("x2", translateX(end_point.x, layout))
       << attribute("y2", translateY(end_point.y, layout))
       << stroke.toString(layout) << emptyElemEnd();
    return ss.str();
  }
  void offset(Point const &offset)
  {
    start_point.x += offset.x;
    start_point.y += offset.y;

    end_point.x += offset.x;
    end_point.y += offset.y;
  }

private:
  Point start_point;
  Point end_point;
};

#endif