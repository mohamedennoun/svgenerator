#ifndef _OPTIONAL_HPP_
#define _OPTIONAL_HPP_
#include <iostream>



template <typename T>
class optional
{
public:
  optional<T>(T const &type)
      : valid(true), type(type) {}
  optional<T>() : valid(false), type(T()) {}
  T *operator->()
  {
    // If we try to access an invalid value, an exception is thrown.
    if (!valid)
      throw std::exception();

    return &type;
  }
  // Test for validity.
  bool operator!() const { return !valid; }

private:
  bool valid;
  T type;
};

#endif