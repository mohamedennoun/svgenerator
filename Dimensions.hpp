#ifndef _DIMENSIONS_HPP_
#define _DIMENSIONS_HPP_

struct Dimensions
{
  Dimensions(double width, double height) : width(width), height(height) {}
  Dimensions(double combined = 0) : width(combined), height(combined) {}
  double width;
  double height;
};

#endif