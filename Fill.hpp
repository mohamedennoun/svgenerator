#ifndef _FILL_HPP_
#define _FILL_HPP_
#include "Serializeable.hpp"
#include "Color.hpp"
#include "svg.hpp"

using namespace svg;

class Fill : public Serializeable
{
public:
  Fill(Color::Defaults color) : color(color) {}
  Fill(Color color = Color::Transparent)
      : color(color) {}
  std::string toString(Layout const &layout) const
  {
    std::stringstream ss;
    ss << attribute("fill", color.toString(layout));
    return ss.str();
  }

private:
  Color color;
};

#endif