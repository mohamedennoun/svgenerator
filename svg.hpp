
#ifndef SVG_HPP
#define SVG_HPP

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include "optional.hpp"
#include "Point.hpp"
#include "Layout.hpp"

namespace svg
{

  // Utility XML/String Functions.
  template <typename T>
  std::string static attribute(std::string const &attribute_name,
                        T const &value, std::string const &unit = "")
  {
    std::stringstream ss;
    ss << attribute_name << "=\"" << value << unit << "\" ";
    return ss.str();
  }
  std::string static elemStart(std::string const &element_name)
  {
    return "\t<" + element_name + " ";
  }
  std::string static elemEnd(std::string const &element_name)
  {
    return "</" + element_name + ">\n";
  }
  std::string static emptyElemEnd()
  {
    return "/>\n";
  }

  optional<Point> static getMinPoint(std::vector<Point> const &points)
  {
    if (points.empty())
      return optional<Point>();

    Point min = points[0];
    for (unsigned i = 0; i < points.size(); ++i)
    {
      if (points[i].x < min.x)
        min.x = points[i].x;
      if (points[i].y < min.y)
        min.y = points[i].y;
    }
    return optional<Point>(min);
  }
  optional<Point> static getMaxPoint(std::vector<Point> const &points)
  {
    if (points.empty())
      return optional<Point>();

    Point max = points[0];
    for (unsigned i = 0; i < points.size(); ++i)
    {
      if (points[i].x > max.x)
        max.x = points[i].x;
      if (points[i].y > max.y)
        max.y = points[i].y;
    }
    return optional<Point>(max);
  }

  // Convert coordinates in user space to SVG native space.
  double static translateX(double x, Layout const &layout)
  {
    if (layout.origin == Layout::BottomRight || layout.origin == Layout::TopRight)
      return layout.dimensions.width - ((x + layout.origin_offset.x) * layout.scale);
    else
      return (layout.origin_offset.x + x) * layout.scale;
  }

  double static translateY(double y, Layout const &layout)
  {
    if (layout.origin == Layout::BottomLeft || layout.origin == Layout::BottomRight)
      return layout.dimensions.height - ((y + layout.origin_offset.y) * layout.scale);
    else
      return (layout.origin_offset.y + y) * layout.scale;
  }
  double static translateScale(double dimension, Layout const &layout)
  {
    return dimension * layout.scale;
  }
};

#endif