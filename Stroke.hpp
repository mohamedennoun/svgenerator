#ifndef _STROKE_HPP_
#define _STROKE_HPP_
#include "Serializeable.hpp"
#include "Color.hpp"
#include "svg.hpp"

using namespace svg;

class Stroke : public Serializeable
{
public:
  Stroke(double width = -1, Color color = Color::Transparent)
      : width(width), color(color) {}
  std::string toString(Layout const &layout) const
  {
    // If stroke width is invalid.
    if (width < 0)
      return std::string();

    std::stringstream ss;
    ss << attribute("stroke-width", translateScale(width, layout)) << attribute("stroke", color.toString(layout));
    return ss.str();
  }

private:
  double width;
  Color color;
};

#endif