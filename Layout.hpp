#ifndef _LAYOUT_HPP_
#define _LAYOUT_HPP_
#include "Dimensions.hpp"
#include "Point.hpp"

// Defines the dimensions, scale, origin, and origin offset of the document.
struct Layout
{
  enum Origin
  {
    TopLeft,
    BottomLeft,
    TopRight,
    BottomRight
  };

  Layout(Dimensions const &dimensions = Dimensions(400, 300), Origin origin = BottomLeft,
         double scale = 1, Point const &origin_offset = Point(0, 0))
      : dimensions(dimensions), scale(scale), origin(origin), origin_offset(origin_offset) {}
  Dimensions dimensions;
  double scale;
  Origin origin;
  Point origin_offset;
};

#endif