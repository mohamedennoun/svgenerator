#ifndef _FONT_HPP_
#define _FONT_HPP_
#include "Serializeable.hpp"
#include "svg.hpp"

using namespace svg;

class Font : public Serializeable
{
public:
  Font(double size = 12, std::string const &family = "Verdana") : size(size), family(family) {}
  std::string toString(Layout const &layout) const
  {
    std::stringstream ss;
    ss << attribute("font-size", translateScale(size, layout)) << attribute("font-family", family);
    return ss.str();
  }

private:
  double size;
  std::string family;
};

#endif